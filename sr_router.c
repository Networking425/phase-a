/**
 *  Simple Router - Phase 1
 *
 *  @author: Hercy (Yunhao Zhang)
 *  @author: Jeremy Mark Norris
 */

/**********************************************************************
 * file:  sr_router.c 
 * date:  Mon Feb 18 12:50:42 PST 2002  
 * Contact: casado@stanford.edu 
 *
 * Description:
 * 
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing. 11
 * 90904102
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"

#define MAC_SIZE ETHER_ADDR_LEN * sizeof(unsigned char)



#define DEBUG_A     1

static int i, j;
/*--------------------------------------------------------------------- 
 * The ARP_Packet linkedlist handles ARP
 *---------------------------------------------------------------------*/
typedef struct ARP_Packet
{
    // TODO: There's gonna be some data entries

    struct ARP_Packet * next;

} ARP_Packet;

ARP_Packet * ARP_List;

/*--------------------------------------------------------------------- 
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 * 
 *---------------------------------------------------------------------*/
void sr_init(struct sr_instance* sr) 
{
    if(DEBUG_A)
    {
        printf("\n*** @ sr_init: Started\n");
        printf("    > Initializing... ");
    }

    /* REQUIRES */
    assert(sr);
    
    /* Add initialization code here! */
    ARP_List = NULL;
    if(DEBUG_A)
    {
        printf("OK\n");
    }
} /* -- sr_init -- */



/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/
void sr_handlepacket(struct sr_instance * sr, 
        uint8_t * packet/* lent */,
        unsigned int len,
        char * interface/* lent */)
{
    if(DEBUG_A)
    {
        printf("\n*** @ sr_handlepacket: Started\n");
    }

    /* REQUIRES */
    assert(sr);
    assert(packet);
    assert(interface);

    if(DEBUG_A)
    {
        printf("    > Received packet of length: %d \n", len);
        printf("    > Interface: %s\n", interface);
        
        printf("    > Username: %s\n", sr->user);
        printf("    > Hostname: %s\n", sr->host);
        printf("    > Template: %s\n", sr->template);
        printf("    > auth_key_fn: %s\n", sr->auth_key_fn);
        printf("    > topo_id: %d\n", sr->topo_id);
    }

    // Get all headers
    struct sr_if * iface = sr_get_interface(sr, interface);
    struct sr_ethernet_hdr * e_hdr = (struct sr_ethernet_hdr *)packet;
    
    //printf("    - ether_type: %d\n", ntohs(e_hdr->ether_type));
    
    if(DEBUG_A)
    {
        printf("    > Checking for ethernet type... ");
    }
    int etherType = ntohs(e_hdr->ether_type);
    
    int sizeOfEhdr = sizeof(struct sr_ethernet_hdr);
    int sizeOfAhdr = sizeof(struct sr_arphdr);
    
    /* --------------------------------------------------
     * ARP
     * -------------------------------------------------- */
    if(etherType == ETHERTYPE_ARP)
    {
        if(DEBUG_A)
        {
            printf("ARP\n");
        }

        //printf("    - Size of e_hdr: %d\n", sizeOfEhdr);
        //printf("    - Size of a_hdr: %d\n", sizeOfAhdr);
        
        if(DEBUG_A)
        {
            printf("    > Checking for ARP type... ");
        }
        struct sr_arphdr * a_hdr = (struct sr_arphdr *)(packet + sizeOfEhdr);
        
        int arpRequet = ntohs(a_hdr->ar_op);
        
        //printf("    - ahdr: %d, %d, op: %d\n", a_hdr->ar_sip, a_hdr->ar_tip, arpRequet);
        
        // Request
        if(arpRequet == ARP_REQUEST)
        {
            if(DEBUG_A)
            {
                printf("REQUEST\n");
            }
            
            // TODO
            struct sr_ethernet_hdr * e_hdr_reply = (struct sr_ethernet_hdr *)malloc(sizeof(struct sr_ethernet_hdr));
            
            int sizeOfEtherHost = sizeof(uint8_t) * ETHER_ADDR_LEN;
            
            memcpy(e_hdr_reply->ether_dhost, e_hdr->ether_shost, sizeOfEtherHost);
            
            memcpy(e_hdr_reply->ether_shost, iface->addr, sizeOfEtherHost);
            e_hdr_reply->ether_type = e_hdr->ether_type;
            
            
            
            struct sr_arphdr * a_hdr_reply = (struct sr_arphdr *)malloc(sizeof(struct sr_arphdr));
            
            

            //printf("    - a_hdr_reply: op: %d\n", ntohs(a_hdr_reply->ar_op));
            
            memcpy(a_hdr_reply, a_hdr, sizeOfAhdr);
            
            a_hdr_reply->ar_op = htons(ARP_REQUEST);//ARP_REPLY);//
            a_hdr_reply->ar_sip = a_hdr->ar_tip;
            a_hdr_reply->ar_tip = a_hdr->ar_sip;
            
            
            if(DEBUG_A)
            {
                printf("    > Original source address: ");
                DebugMAC(a_hdr_reply->ar_sha);
                printf("\n");
            }
            //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
           
            
            //memset(a_hdr_reply->ar_sha, '\0', MAC_SIZE);
            memcpy(a_hdr_reply->ar_sha, iface->addr, MAC_SIZE);
            
            //memset(a_hdr_reply->ar_tha, '\0', MAC_SIZE);
            memcpy(a_hdr_reply->ar_tha, a_hdr->ar_sha, MAC_SIZE);
            
            
            if(DEBUG_A)
            {
                printf("    > Source ip: %d\n", ntohs(a_hdr_reply->ar_sip));
                printf("    > iFace ip: %d\n", ntohs(iface->ip));
            
                printf("    > Source address: ");
                DebugMAC(a_hdr_reply->ar_sha);
                printf("\n");
                //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
            
                printf("    > iFace address: ");
                DebugMAC(iface->addr);
                printf("\n");
                //printf(" <<<%s>>>\n", iface->addr);
            }
            
            
            //printf("    - a_hdr_reply: op: %d\n", ntohs(a_hdr_reply->ar_op));
            
            
            // Send a packet
            
            //len - sizeof(c_packet_ethernet_header) + sizeof(struct sr_ethernet_hdr)
            int length = sizeOfEhdr + sizeOfAhdr;
            uint8_t * aPacket = (uint8_t *)malloc(length);
            
            
            memcpy(aPacket, e_hdr_reply, sizeOfEhdr);
            //*aPacket = (*aPacket) << sizeOfAhdr;
            memcpy(aPacket + sizeOfEhdr, a_hdr_reply, sizeOfAhdr);
            //memcpy(aPacket, a_hdr_reply, sizeOfAhdr);
            
            //len - sizeOfEhdr + sizeof(struct sr_ethernet_hdr)
            sr_send_packet(sr, aPacket, length, iface->name);
            

        }
        
        
        
        // Reply
        else if(arpRequet == ARP_REPLY)
        {
            if(DEBUG_A)
            {
                printf("REPLY\n");
            }
            printf(">>> > We got the reply!!!\n");
            
            if(DEBUG_A)
            {
                printf("    > Original source address: ");
                DebugMAC(a_hdr->ar_sha);
                printf("\n");
                
                printf("    > Source ip: %d\n", ntohs(a_hdr->ar_sip));
                printf("    > iFace ip: %d\n", ntohs(iface->ip));
                
                printf("    > Source address: ");
                DebugMAC(a_hdr->ar_sha);
                printf("\n");
                //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
                
                printf("    > iFace address: ");
                DebugMAC(iface->addr);
                printf("\n");
                //printf(" <<<%s>>>\n", iface->addr);
            }
            // TODO
        }
        
        
        // Unknown
        else
        {
            if(DEBUG_A)
            {
                printf("ERROR\n");
            }
            printf("ERROR: Unknow ARP response");
        }
    }
    
    
    /* --------------------------------------------------
     * IP
     * -------------------------------------------------- */
    else if(etherType == ETHERTYPE_IP)
    {
        if(DEBUG_A)
        {
            printf("IP\n");
        }
        
        struct ip * ip_hdr = (struct ip *)(packet + sizeOfEhdr);
        
        if(DEBUG_A)
        {
            printf("    > Header length: %d\n", ip_hdr->ip_hl);
            printf("    > Version: %d\n", ip_hdr->ip_v);
            
            printf("    > Source address: %s\n", inet_ntoa(ip_hdr->ip_src));
            printf("    > Destination address: %s\n", inet_ntoa(ip_hdr->ip_dst));
            
            
            printf("    > Type of service: %d\n", ip_hdr->ip_tos);
            printf("    > Total length: %d\n", ip_hdr->ip_len);
            
            printf("    > Identification: %d\n", ip_hdr->ip_id);
            printf("    > Fragment offset field: %d\n", ip_hdr->ip_off);
            
            printf("    > Protocol: %d\n", ip_hdr->ip_p);
            printf("    > Checksum: %d\n", ip_hdr->ip_sum);
            
        }
        
        
        /* --------------------------------------------------
         * ICMP
         * -------------------------------------------------- */
        if(ip_hdr->ip_p == IPPROTO_ICMP)
        {
            if(DEBUG_A)
            {
                printf("    > Protocol: ICMP\n");
            }
        }
        else if(ip_hdr->ip_p == 6)
        {
            if(DEBUG_A)
            {
                printf("    > Protocol: TCP\n");
            }
        }
        
        
        //struct sr_arphdr * a_hdr = (struct sr_arphdr *)(packet + sizeOfEhdr);
    }
    
    
    
    /* --------------------------------------------------
     * ICMP
     * -------------------------------------------------- */
    else if(etherType == IPPROTO_ICMP)
    {
        if(DEBUG_A)
        {
            printf("ICMP\n");
        }
        
    }
    
    
    /* --------------------------------------------------
     * Unknown
     * -------------------------------------------------- */
    else
    {
        printf("    - etherType: %d\n", etherType);
        if(DEBUG_A)
        {
            printf("Unknown\n");
        }
    }
    
    
    
    // Send a packet
    //sr_send_packet(sr, packet, len, interface);
    
}/* end sr_ForwardPacket */


/*--------------------------------------------------------------------- 
 * Method:
 *
 *---------------------------------------------------------------------*/
